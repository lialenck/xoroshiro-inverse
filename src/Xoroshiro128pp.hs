{-# LANGUAGE GADTs, StandaloneDeriving #-}

module Xoroshiro128pp (
    Xoroshiro128pp(..),
    nextState,
    nthState
) where

import Data.Bits (Bits, xor, rotateL, shiftL)

data Xoroshiro128pp v where
    Xos :: Bits v => v -> v -> Xoroshiro128pp v
deriving instance Eq v   => Eq   (Xoroshiro128pp v)
deriving instance Show v => Show (Xoroshiro128pp v)
deriving instance Ord v  => Ord  (Xoroshiro128pp v)

nextState :: Xoroshiro128pp v -> Xoroshiro128pp v
nextState (Xos i j) = let j' = j `xor` i
    in Xos
        ((i `rotateL` 49) `xor` j' `xor` (j' `shiftL` 21))
        (j' `rotateL` 28)

-- |Naive version. can be calculated in O(log(n)), but only becomes
-- faster for very large n, and the algorithm is out of scope for this module.
nthState :: Int -> Xoroshiro128pp v -> Xoroshiro128pp v
nthState i rng = iterate nextState rng !! i
