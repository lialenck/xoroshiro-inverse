module Xorvec (
    Xorvec64,
    xovInitialStates,
    Xormat(..),
    xovsToMatrix,
    matrixVectorMult,
) where

-- TODO use Data.Vector instead :)
import Control.Monad
import Data.Bits
import Data.Bool (bool)
import Data.Word
import Data.List (foldl')
import qualified Data.IntSet as S
import qualified Data.Vector as VB
import qualified Data.Vector.Unboxed as V
import qualified Data.Vector.Unboxed.Mutable as VM

data Xorvec64 = XV (VB.Vector S.IntSet) deriving (Show, Eq, Ord)

instance Bits Xorvec64 where
    xor (XV a) (XV b) = XV
        $ VB.zipWith (\av bv -> S.union av bv S.\\ S.intersection av bv) a b

    -- TODO implement these on top of VB.backpermute
    rotate (XV v) n = XV $ VB.backpermute v $ VB.generate 64 (flip mod 64 . (+ n))
    shift (XV v) n = XV $ VB.generate 64 $ \i ->
        let oldI = i + n
        in bool (v VB.! oldI) S.empty (oldI < 0 || oldI > 63)

    (.&.) = undefined
    (.|.) = undefined
    complement = undefined
    bitSize = undefined
    bitSizeMaybe = undefined
    isSigned = undefined
    testBit = undefined
    bit = undefined
    popCount = undefined

xovInitialStates :: [Xorvec64]
xovInitialStates = flip map [0,64..] $ \st ->
    XV $ VB.generate 64 $ \i -> S.singleton (i + st)

data Xormat = Xom (V.Vector Word64) deriving (Eq, Ord)
instance Show Xormat where
    show (Xom ws) = unlines $ map showRow [0..sideLength-1]
      where sideLength = floor $ sqrt $ fromIntegral $ 64 * V.length ws
            rowElems = sideLength `div` 64
            showRow r = V.foldl' (\o n -> o ++ showWord n) "" $ V.slice (r * rowElems) rowElems ws
            showWord w = foldl' (\o n -> o ++ bool "0" "1" (testBit w n)) "" [63,62..0]

xovsToMatrix :: [Xorvec64] -> Xormat
xovsToMatrix l = Xom $
    V.generate (64 * length l ^ 2) $ \i ->
        let (row, colOff) = i `divMod` length l -- position in implied matrix
            (rowOuter, rowInner) = row `divMod` 64
            (XV arr) = l !! rowOuter          -- vector containing the bits infos we want
            bits = arr VB.! rowInner          -- bit infos we want for the current row
        in foldl' (updateWord bits) 0 [colOff * 64 .. colOff * 64 + 63]
  where updateWord :: S.IntSet -> Word64 -> Int -> Word64
        updateWord bits w col =
            if col `S.member` bits
            then w .|. bit (63 - col `mod` 64)
            else w

matrixVectorMult :: Xormat -> V.Vector Word64 -> V.Vector Word64
matrixVectorMult (Xom mat) v = V.create $ do
    let sideLength = floor $ sqrt $ fromIntegral $ 64 * V.length mat
        rowElems = sideLength `div` 64
    when (V.length v * 64 /= sideLength) $ error "matrixVectorMult: dimension mismatch"
    rv <- VM.replicate (V.length v) 0

    forM_ [0..sideLength - 1] $ \row -> do
        let (outerOff, innerOff) = row `divMod` 64

            -- the intermediate vectors from slicing/zipping could be avoided,
            -- but I'm hoping this is just as fast
            rowMultBits = V.zipWith (.&.) v $ V.slice (row * rowElems) rowElems mat
            bitValue = 1 .&. popCount (V.foldl1 xor rowMultBits)
            bitToOr = fromIntegral bitValue `shiftL` (63 - innerOff)

        VM.unsafeModify rv (.|. bitToOr) outerOff

    return rv

instance Semigroup Xormat where
    (<>) = matrixMatrixMult

matrixMatrixMult :: Xormat -> Xormat -> Xormat
matrixMatrixMult (Xom m1) (Xom m2) = Xom $ V.create $ do
    let myFoldl' st l f = foldl' f st l

    when (V.length m1 /= V.length m2) $ error "matrixMatrixMult: dimension mismatch"
    let sideLength = floor $ sqrt $ fromIntegral $ 64 * V.length m1
        rowElems = sideLength `div` 64

    rm <- VM.replicate (V.length m1) 0

    forM_ [0..sideLength - 1] $ \row ->
        forM_ [0..sideLength - 1] $ \col -> do
            -- "outer/inner column/row"
            let (oc, ic) = col `divMod` 64

            let bitValue = (.&. 1) $ myFoldl' 0 [0..sideLength - 1] $ \o off ->
                    -- the left word will be the same 64x in a row, so this could be
                    -- deduplicated. Should be relevant for optimization.
                    let (leftColO, leftColI) = off `divMod` 64
                    -- words containing the bits we care about
                        leftWord = V.unsafeIndex m1 (row * rowElems + leftColO)
                        rightWord = V.unsafeIndex m2 (off * rowElems + oc)

                    -- the lowest bit of these is the one we want. we don't extract it
                    -- with .&. until later, to save one binary and
                        leftBit = leftWord `shiftR` (63 - leftColI)
                        rightBit = rightWord `shiftR` (63 - ic)

                    in xor o $ 1 .&. leftBit .&. rightBit

            let bitToOr = bitValue `shiftL` (63 - ic)
            VM.unsafeModify rm (.|. bitToOr) (row * rowElems + oc)

    return rm
