{-# LANGUAGE LambdaCase #-}

module Main (main) where

import Numeric (showBin)

import Control.Monad
import Data.Bits
import Data.Function ((&))
import Data.Semigroup (stimes)
import Data.Word
import qualified Data.Vector.Unboxed as V
import qualified Data.Vector.Unboxed.Mutable as VM

import Xorvec
import Xoroshiro128pp

-- not taking the second argument directly can dramatically reduce time complexity
-- for some users. the same could be achieved with taking it directly and using
-- {-# INLINE #-}, but I believe this is more robust.
logarithmicNthState :: Int -> Xoroshiro128pp Word64 -> Xoroshiro128pp Word64
logarithmicNthState n =
    let trackedRng = Xos (xovInitialStates !! 0) (xovInitialStates !! 1)
        (Xos t1 t2) = nextState trackedRng
        mat = xovsToMatrix [t1, t2]
        nthMatrix = stimes n mat
    in \(Xos i j) ->
        let targetState = matrixVectorMult nthMatrix $ V.fromList [i, j]
            (i', j') = V.toList targetState & \case
                [a, b] -> (a, b)
                _ -> error "logarithmicNthState: impossible!"
        in Xos i' j'

-- gauss elimination
invertMatrix :: Xormat -> Xormat
invertMatrix (Xom mat) = Xom $ V.create $ do
    let sideLength = floor $ sqrt $ fromIntegral $ 64 * V.length mat
        rowElems = sideLength `div` 64

    -- input/output matrix, with the output matrix being initialized to identity
    im <- V.thaw mat
    om <- VM.replicate (V.length mat) (0 :: Word64)
    forM_ [0..sideLength - 1] $ \i -> do
        let (oc, ic) = i `divMod` 64
        VM.unsafeWrite om (i * rowElems + oc) $ bit (63 - ic)

    -- utility functions
    let addRow i j matSrc matDst = forM_ [0..rowElems - 1] $ \off -> do
            r <- VM.unsafeRead matSrc $ i * rowElems + off
            VM.unsafeModify matDst (xor r) $ j * rowElems + off
        mayAddBoth mayI j = case mayI of
            Nothing -> return $ Just j
            Just i -> addRow i j im im >> addRow i j om om >> return mayI
        nthBitSet r n = do
            let (oc, ic) = n `divMod` 64
            flip testBit (63 - ic) <$> VM.unsafeRead im (r * rowElems + oc)
        leadingZeros r = lzGo r 0
        lzGo r n =
            if n >= rowElems
            then return sideLength
            else VM.unsafeRead im (r * rowElems + n) >>= \w ->
                if countLeadingZeros w < 64
                then return $ countLeadingZeros w + n * 64
                else lzGo r (n + 1)

    forM_ [0..sideLength - 1] $ \nthPass -> do
        let go oi j = do
                lz <- leadingZeros j
                if lz == nthPass
                then mayAddBoth oi j
                else return oi
        foldM go Nothing [0..sideLength - 1]

    forM_ [0..sideLength - 1] $ \r -> do
        b <- (>= sideLength) <$> leadingZeros r
        when b $ error "invertMatrix: given uninvertible matrix"

    -- the sorting is done in a block, as i'm hoping it makes the garbage collector
    -- more eager to collect the temporary vectors
    do
        -- these will be our sorted matrices
        im' <- VM.replicate (V.length mat) (0 :: Word64)
        om' <- VM.replicate (V.length mat) (0 :: Word64)
        -- just copy each row to the place it belongs to
        forM_ [0..sideLength - 1] $ \r -> do
            n <- leadingZeros r
            addRow r n im im'
            addRow r n om om'
        -- copy these matrices back to the original ones
        VM.move im im'
        VM.move om om'

    -- clear out the top right triangle, which is the only difference to identity
    forM_ [1..sideLength - 1] $ \pass ->
        forM_ [0..pass - 1] $ \row -> do
            b <- nthBitSet row pass
            when b $ void $ do
                mayAddBoth (Just pass) row

    return om

toGray :: Bits v => v -> v
toGray i = xor i $ i `shiftR` 1

lpad :: a -> Int -> [a] -> [a]
lpad p n l = replicate (n - length l) p ++ l

main :: IO ()
main = do
    let mat = xovsToMatrix [toGray $ xovInitialStates !! 0]
        imat = invertMatrix mat
    print mat
    print imat
